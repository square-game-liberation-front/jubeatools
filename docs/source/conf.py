# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sys
from pathlib import Path

import toml

project_root = Path(__file__).parents[2]
sys.path.append(str(project_root))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "jubeatools"
copyright = "2022, Stepland"
author = "Stepland"

with open(project_root / "pyproject.toml") as f:
    pyproject = toml.load(f)

release = pyproject["tool"]["poetry"]["version"]

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser",
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx"
]

templates_path = ["_templates"]
exclude_patterns = []

nitpicky = True

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None)
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"
html_static_path = ["_static"]

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

myst_heading_anchors = 2

myst_enable_extensions = [
    # "amsmath",
    "colon_fence",
    "deflist",
    # "dollarmath",
    "fieldlist",
    # "html_admonition",
    # "html_image",
    # "linkify",
    # "replacements",
    # "smartquotes",
    # "strikethrough",
    # "substitution",
    # "tasklist",
]

# Autodoc options
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#confval-autodoc_mock_imports

autodoc_mock_imports = [
    "more_itertools",
    "sortedcontainers",
    "parsimonious",
    "constraint",
    "construct",
    "construct_typed",
    "simplejson",
    "marshmallow",
    "marshmallow_dataclass"
]

autodoc_default_options = {
    "members": None,  # document all members
    "member-order": "bysource", # respect source declaration order
    "undoc-members": True,  # add an entry for members that lack docstrings
}


# Options for furo, the theme I currently use
# See https://pradyunsg.me/furo/customisation/
html_theme_options = {
    "footer_icons": [
        {
            "name": "GitLab",
            "url": pyproject["tool"]["poetry"]["repository"],
            "html": """
                <svg
                    stroke="currentColor"
                    fill="currentColor"
                    viewBox="0 0 193.05176 185.94366"
                    width="193.05176"
                    height="185.94366"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path d="m 189.35957,73.712043 -0.27,-0.69 -26.14,-68.2200003 a 6.81,6.81 0 0 0 -2.69,-3.24 7,7 0 0 0 -8,0.43 7,7 0 0 0 -2.32,3.52 l -17.65,54.0000003 H 60.819567 l -17.65,-54.0000003 a 6.86,6.86 0 0 0 -2.32,-3.53 7,7 0 0 0 -8,-0.43 6.87,6.87 0 0 0 -2.69,3.24 L 3.9695671,72.982043 l -0.26,0.69 A 48.54,48.54 0 0 0 19.809567,129.77204 l 0.09,0.07 0.24,0.17 39.82,29.82 19.7,14.91 12,9.06 a 8.07,8.07 0 0 0 9.760003,0 l 12,-9.06 19.7,-14.91 40.06,-30 0.1,-0.08 a 48.56,48.56 0 0 0 16.08,-56.039997 z"/>
                </svg>
            """,
            "class": "",
        },
    ],
}
