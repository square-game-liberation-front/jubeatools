# Loaders and dumpers

## Collections

```{eval-rst}
.. autodata:: jubeatools.formats.loaders_and_dumpers.LOADERS
   :no-value:
.. autodata:: jubeatools.formats.loaders_and_dumpers.DUMPERS
   :no-value:
```

## Reference

### jubeat analyzer

```{eval-rst}
.. autofunction:: jubeatools.formats.jubeat_analyser.memo.load::load_memo

.. autofunction:: jubeatools.formats.jubeat_analyser.memo.dump.dump_memo

   Default Filename Template
      ``{title} {difficulty_number}.txt``

   :kwarg bool circle_free: Use circle-free symbols for long note ends

.. autofunction:: jubeatools.formats.jubeat_analyser.memo1.load::load_memo1

.. autofunction:: jubeatools.formats.jubeat_analyser.memo1.dump::dump_memo1

   Default Filename Template
      ``{title} {difficulty_number}.txt``

   :keyword bool circle_free: Use circle-free symbols for long note ends

.. autofunction:: jubeatools.formats.jubeat_analyser.memo2.load::load_memo2

.. autofunction:: jubeatools.formats.jubeat_analyser.memo2.dump::dump_memo2

   Default Filename Template
      ``{title} {difficulty_number}.txt``

   :keyword bool circle_free: Use circle-free symbols for long note ends

.. autofunction:: jubeatools.formats.jubeat_analyser.mono_column.load::load_mono_column

.. autofunction:: jubeatools.formats.jubeat_analyser.mono_column.dump::dump_mono_column

   Default Filename Template
      ``{title} {difficulty_number}.txt``

   :keyword bool circle_free: Use circle-free symbols for long note ends
```

### konami

```{eval-rst}
.. autofunction:: jubeatools.formats.konami.eve.load::load_eve

   :kwarg int beat_snap: Snap all events to nearest 1/``beat_snap`` beat

.. autofunction:: jubeatools.formats.konami.eve.dump::dump_eve

   Default Filename Template
      ``{difficulty:l}.eve``

.. autofunction:: jubeatools.formats.konami.jbsq.load::load_jbsq

   :kwarg int beat_snap: Snap all events to nearest 1/``beat_snap`` beat

.. autofunction:: jubeatools.formats.konami.jbsq.dump::dump_jbsq

   Default Filename Template
      ``seq_{difficulty:l}.jbsq``
```

### malody

```{eval-rst}
.. autofunction:: jubeatools.formats.malody.load::load_malody

.. autofunction:: jubeatools.formats.malody.dump::dump_malody

   Default Filename Template
      ``{difficulty:l}.mc``
```

### memon

```{eval-rst}
.. autofunction:: jubeatools.formats.memon.v0.load::load_memon_legacy

   :kwarg bool merge: When called on a folder, try to merge all the .memon
      files found into a single :py:class:`Song <jubeatools.song.Song>` object

.. autofunction:: jubeatools.formats.memon.v0.dump::dump_memon_legacy

   Default Filename Template
      ``{title}.memon``

.. autofunction:: jubeatools.formats.memon.v0.load::load_memon_0_1_0

   :kwarg bool merge: When called on a folder, try to merge all the .memon
      files found into a single :py:class:`Song <jubeatools.song.Song>` object

.. autofunction:: jubeatools.formats.memon.v0.dump::dump_memon_0_1_0

   Default Filename Template
      ``{title}.memon``

.. autofunction:: jubeatools.formats.memon.v0.load::load_memon_0_2_0

   :kwarg bool merge: When called on a folder, try to merge all the .memon
      files found into a single :py:class:`Song <jubeatools.song.Song>` object

.. autofunction:: jubeatools.formats.memon.v0.dump::dump_memon_0_2_0

   Default Filename Template
      ``{title}.memon``

.. autofunction:: jubeatools.formats.memon.v0.load::load_memon_0_3_0

   :kwarg bool merge: When called on a folder, try to merge all the .memon
      files found into a single :py:class:`Song <jubeatools.song.Song>` object

.. autofunction:: jubeatools.formats.memon.v0.dump::dump_memon_0_3_0

   Default Filename Template
      ``{title}.memon``

.. autofunction:: jubeatools.formats.memon.v1.load::load_memon_1_0_0

   :kwarg bool merge: When called on a folder, try to merge all the .memon
      files found into a single :py:class:`Song <jubeatools.song.Song>` object

.. autofunction:: jubeatools.formats.memon.v1.dump::dump_memon_1_0_0

   Default Filename Template
      ``{title}.memon``
```


### yubiosi

```{eval-rst}
.. autofunction:: jubeatools.formats.yubiosi.load::load_yubiosi_1_0

.. autofunction:: jubeatools.formats.yubiosi.dump::dump_yubiosi_1_0

   Default Filename Template
      ``{title}.txt``

.. autofunction:: jubeatools.formats.yubiosi.load::load_yubiosi_1_5

.. autofunction:: jubeatools.formats.yubiosi.dump::dump_yubiosi_1_5

   Default Filename Template
      ``{title}.txt``

.. autofunction:: jubeatools.formats.yubiosi.load::load_yubiosi_2_0

.. autofunction:: jubeatools.formats.yubiosi.dump::dump_yubiosi_2_0

   Default Metadata Filename Template
      ``{title}.ybi``

   Default Chart Filename Template
      ``{title}[{difficulty}].ybh``
```