# Converting charts with jubeatools

This page explains how to use jubeatools via command line for people who are
unfamiliar with the terminal.

Make sure you [installed Python and jubeatools](<how to install jubeatools.md>)
before reading this page.

**jubeatools has no graphical user interface**, to use it, you have to type
commands in a terminal. If you are completely new to the terminal, this next
section was made for you, otherwise you can skip it.

## Primer on the terminal

The terminal is a text interface to your computer, you type in a command,
it does something in response, and maybe displays some text to tell you what
it just did.

### It's basically File Explorer but in text

At any given time while using the terminal you will be "in" a folder, just like
when using File Explorer on Windows.

On Windows, in both `cmd.exe` and PowerShell, the current directory
(this folder you are *in*) is displayed on the left as part of the *prompt*

```{figure-md}
:class: myclass

![](_static/current_directory_in_cmd.exe's_prompt.png)

Here as you can see I'm inside `C:\Users\Stepland`
```

On Linux and macOS, the current directory might not be displayed in the prompt.
You can use the `pwd` command to display it (`pwd` as in **P**rint **W**orking
**D**irectory)

### Navigating with `dir`/`ls` and `cd`

Now that you know how to see where you are in, let's learn how to move around
and see what things are there.

To see what's in the current folder, use :

- `dir` on Windows (**dir**ectory)
- `ls` on macOS/Linux (**l**i**s**t)

To move to a different folder, use `cd` (**c**hange **d**irectory) (same on all
3 OSes).

`cd` accepts two kinds of paths :
- absolute paths (paths that start with `C:\` on Windows or `/` on macOS/Linux)
- relative paths (relative to the current folder) : if there is a sub-folder
  named `Photos` in your home folder *and* you are already in you home folder
  on the terminal, you can just do `cd Photos` instead of giving the absolute
  path to the `Photos` folder

On all 3 OSes, there's a special "fake" folder called `..` (double dot). It's
there to help you move *out* of the current folder. You can do `cd ..` from
anywhere and it will take you one folder up the hirerarchy.

### Tab triggers autocomplete

Typing long paths is tedious, luckily the terminal can help you. Start typing
your command (`cd ...something...`) then hit Tab before finishing, the terminal will
try to the fill in the rest of the folder or file name you were trying to type,
this can be chained multiple times in a row to type a longer path :

- Type the first few characters of the folder
- Tab (maybe more than once if several folders match)
- Type the first few characters of the sub-folder
- Tab again

etc etc ...

## Using jubeatools in a terminal

Now that you know the commands to move around, let's learn how to use
jubeatools itself as a command.

jubeatools expects arguments like this :

```console
$ jubeatools (input) (output) -f (output format) (options)
```

Let's break this down :

- `$` is a common way to represent a terminal *prompt* in computer litterature,
  it is not part of the actual command, you don't have to type it. It's just
  some sort of punctuation mark to remind you that whatever comes after it is a
  command, and can be typed in a terminal.
- `jubeatools` is the command
- `(input)` is the path of the chart file you want to convert
- `(output)` is the path of the *converted* chart file you want to create
- `-f (output format)` is the output format you want jubeatools to use
- `(options)` are the extra options you might want to use (see [](cli.md))

### Example

Say you have a memo file called `sigsig.txt` and you want to convert it to
memon 1.0.0, then you would open up a terminal, navigate to the folder where
`sigsig.txt` is, then type the following command :

```console
$ jubeatools sigsig.txt sigsig.memon -f memon:v1.0.0
```

This will create a file called `sigsig.memon` in the same folder.

### Formats

Each format jubeatools supports has a precise name you need to use for the
`-f` option :

|                 |                      | name           |
|-----------------|----------------------|----------------|
| memon           | v1.0.0               | `memon:v1.0.0` |
|                 | v0.3.0               | `memon:v0.3.0` |
|                 | v0.2.0               | `memon:v0.2.0` |
|                 | v0.1.0               | `memon:v0.1.0` |
|                 | legacy               | `memon:legacy` |
| jubeat analyser | #memo2               | `memo2`        |
|                 | #memo1               | `memo1`        |
|                 | #memo                | `memo`         |
|                 | mono-column (1列形式) | `mono-column`  |
| jubeat (arcade) | .eve                 | `eve`          |
| jubeat plus     | .jbsq                | `jbsq`         |
| malody          | .mc (Pad Mode)       | `malody`       |
| yubiosi         | v1.0                 | `yubiosi:v1.0` |
|                 | v1.5                 | `yubiosi:v1.5` |
|                 | v2.0                 | `yubiosi:v2.0` |

### Options

Options are documented here : [](cli.md)