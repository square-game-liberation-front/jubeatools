import argparse
import subprocess

import toml

parser = argparse.ArgumentParser()
parser.add_argument(
    "rule",
    help="either a semver string or a bump rule, will be passed to poetry",
)
parser.add_argument("--no-commit", dest="commit", action="store_false")
args = parser.parse_args()

subprocess.run(["poetry", "version", args.rule], check=True)

with open("pyproject.toml") as f:
    pyproject = toml.load(f)

version = pyproject["tool"]["poetry"]["version"]
repo_url = pyproject["tool"]["poetry"]["repository"]

with open("jubeatools/info.py", mode="w") as f:
    f.write(f'__version__ = "{version}"\n')
    f.write(f'__repository_url__ = "{repo_url}"\n')

if args.commit:
    subprocess.run(["git", "reset"])
    subprocess.run(
        ["git", "add", "pyproject.toml", "jubeatools/info.py"], check=True
    )
    subprocess.run(["git", "commit", "-m", f"Bump version to {version}"])
    subprocess.run(["git", "tag", f"v{version}"])
